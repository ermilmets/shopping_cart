from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, TemplateView
from .models import Product, Category


# Create your views here.
def home(request):
    context = []
    return render(request, 'home.html', context)


class MainHomeView(ListView):
    model = Product
    template_name = 'home.html'
    context_object_name = 'products'

    def get_content_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = Category.objects.all()

        return context  # returning context that has 'category' added


class ProductView(DetailView):   # or TemplateView? ListView?
    model = Product
    template_name = 'products.html'
    context_object_name = 'products'

    def get_content_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = Category.objects.all()

        return context  # returning context that has 'category' added






